import { serve } from 'https://deno.land/std@0.170.0/http/server.ts';
import { Server } from 'https://deno.land/x/socket_io@0.2.0/mod.ts';

const io = new Server({
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

io.on("connection", async (socket) => {
  console.log(`socket ${socket.id} connected`);
  handleDisconnect(socket);
  while (true) {
    await sleep(1000);
    socket.emit("dashboard", "kekW");
  }  
});

const sleep = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const handleDisconnect = (socket) => {
  socket.on("disconnect", (reason) => {
    console.log(`socket ${socket.id} disconnected due to ${reason}`);
  });
};

await serve(io.handler(), {
  port: 3000,
});
