import { Component } from '@angular/core';
import {SocketService} from "./socket.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  constructor(private readonly socketService: SocketService) {
    this.socketService.createSocket({user: 'woot', password: 'woot'})
  }

}
