import {Component} from '@angular/core';
import {DashboardService} from "./dashboard.service";
import {Observable} from "rxjs";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent {

  public dashboardMessages: Observable<string[]>;
  constructor(private readonly dashboardService: DashboardService) {
    this.dashboardMessages = this.dashboardService.getMessagesObservable();
  }

}
