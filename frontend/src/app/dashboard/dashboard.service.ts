import { Injectable } from '@angular/core';
import {SocketService} from "../socket.service";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private readonly dashboardMessages = new BehaviorSubject<string[]>([]);

  constructor(private readonly socketService: SocketService) {
    const socket = this.socketService.getSocket();
    socket.on("dashboard", (data: string) => {
      const messages = this.getMessages();
      messages.push(data);
    });
  }

  public getMessagesObservable() {
    return this.dashboardMessages.asObservable();
  }

  private getMessages() {
    return this.dashboardMessages.getValue();
  }
}
