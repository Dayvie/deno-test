import { Injectable } from '@angular/core';
import {Credentials} from "./auth/credentials";
import {io} from "socket.io-client";

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private readonly server = 'http://localhost:3000/';
  private socket: any;

  public createSocket(credentials: Credentials): any {
    if (this.socket) {
      this.socket.close();
    }
    this.socket = io(this.server, {credentials} as any);
    return this.socket;
  }

  public getSocket(): any {
    return this.socket;
  }

  public getSocketID(): string {
    if (this.socket) {
      return this.socket.io.engine.id;
    }
    throw new Error("No socket instantiated");
  }
}


